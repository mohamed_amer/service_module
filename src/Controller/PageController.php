<?php

namespace Drupal\servicemodule\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Provides route responses for the Example module.
 */
class PageController extends ControllerBase {

  /**
   * Returns a simple page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function servicecall(){
    $service = \Drupal::service("servicemodule.service");
    $service->hello_service();
    $element = array(
      '#markup' => $service->hello_service(),
    );
    return $element;
  }

  public function welcome(){

    $element = array(
      '#markup' => 'Hello, world',
    );
    return $element;
  }

}
?>